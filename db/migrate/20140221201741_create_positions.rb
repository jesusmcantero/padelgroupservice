class CreatePositions < ActiveRecord::Migration
  def change
    create_table :positions do |t|
      t.text :label
      t.text :description

      t.timestamps
    end
  end
end
