class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.text :label
      t.text :description
      t.references :level, index: true
      t.references :level, index: true

      t.timestamps
    end
  end
end
