class CreateCourtTypes < ActiveRecord::Migration
  def change
    create_table :court_types do |t|
      t.text :label
      t.text :description

      t.timestamps
    end
  end
end
