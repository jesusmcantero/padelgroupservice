class CreateClubs < ActiveRecord::Migration
  def change
    create_table :clubs do |t|
      t.text :name
      t.text :description
      t.float :longitude
      t.float :latitude

      t.timestamps
    end
  end
end
