class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.references :level, index: true
      t.references :level, index: true
      t.timestamps :init_date
      t.timestamps :end_date
      t.references :club, index: true

      t.timestamps
    end
  end
end
