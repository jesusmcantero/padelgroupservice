# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140308162544) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "clubs", force: true do |t|
    t.text     "name"
    t.text     "description"
    t.float    "longitude"
    t.float    "latitude"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "clubs_courts", id: false, force: true do |t|
    t.integer "club_id",  null: false
    t.integer "court_id", null: false
  end

  create_table "clubs_matches", id: false, force: true do |t|
    t.integer "match_id", null: false
    t.integer "club_id",  null: false
  end

  create_table "clubs_services", id: false, force: true do |t|
    t.integer "club_id",    null: false
    t.integer "service_id", null: false
  end

  create_table "court_types", force: true do |t|
    t.text     "label"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "groups", force: true do |t|
    t.text     "label"
    t.text     "description"
    t.integer  "level_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "groups", ["level_id"], name: "index_groups_on_level_id", using: :btree

  create_table "groups_players", id: false, force: true do |t|
    t.integer "player_id", null: false
    t.integer "group_id",  null: false
  end

  create_table "levels", force: true do |t|
    t.text     "label"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "matches", force: true do |t|
    t.integer  "level_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "club_id"
  end

  add_index "matches", ["club_id"], name: "index_matches_on_club_id", using: :btree
  add_index "matches", ["level_id"], name: "index_matches_on_level_id", using: :btree

  create_table "matches_players", id: false, force: true do |t|
    t.integer "player_id", null: false
    t.integer "match_id",  null: false
  end

  create_table "players", force: true do |t|
    t.text     "nickName"
    t.text     "email"
    t.text     "phone"
    t.text     "password"
    t.integer  "position_id"
    t.integer  "level_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "players", ["level_id"], name: "index_players_on_level_id", using: :btree
  add_index "players", ["position_id"], name: "index_players_on_position_id", using: :btree

  create_table "positions", force: true do |t|
    t.text     "label"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "services", force: true do |t|
    t.text     "label"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
